import React from 'react';
import './Button.less';

const Button = () => {
    return  (
        <section className='clickButton'>
            <button id = 'search'>Google Search</button>
            <button id = 'luck'>I'm Feeling Lucky</button>
        </section>
    );
};

export default Button;