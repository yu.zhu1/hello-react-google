import React from 'react';
import './Footer.less';

const Footer = () => {
    return (
        <nav className='settings'>
            <p>Hong Kong</p><hr/>
            <ul className='left'>
                <li><a href = "">Advertising</a></li>
                <li><a href = "">Business</a></li>
                <li><a href = "">About</a></li>
                <li><a href = "">How Search Works</a></li>
            </ul>

            <ul className='right'>
                <li><a href = "">Privacy</a></li>
                <li><a href = "">Terms</a></li>
                <li><a href = "">Settings</a></li>
            </ul>
        </nav>
    );
};

export default Footer;