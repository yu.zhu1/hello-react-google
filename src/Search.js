import React from 'react';
import { MdSettingsVoice } from 'react-icons/md';
import { MdSearch } from 'react-icons/md';
import './Search.less';

const Search = () => {
    return (
        <section className='search'>
            <header>
                <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                     alt="Google Logo"/>
            </header>
            <section>
                    <MdSearch className = 'mdSearch'/>
                    <input placeholder = "Search Google or type a URL"/>
                    <MdSettingsVoice className = 'mdSettingVoice'/>
            </section>
        </section>
    );
};

export default Search;