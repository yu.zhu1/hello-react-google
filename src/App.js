import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Button from './Button';
import Nav from "./Nav";
import Footer from "./Footer";

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Button/>
      <Nav/>
      <Footer/>
    </div>
  );
};

export default App;